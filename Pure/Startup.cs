﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pure.Startup))]
namespace Pure
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
